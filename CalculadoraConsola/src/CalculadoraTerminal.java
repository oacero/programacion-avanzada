import java.util.Scanner;

public class CalculadoraTerminal {
    public static void main(String[] args){
        Scanner terminal = new Scanner(System.in);
        while(true){
            System.out.println("***** Calculadora Terminal 2.0 *****");
            // Menu principal
            dibujarMenu();
            try {
                var operacion = Integer.parseInt(terminal.nextLine());

                // Validar opciones a ejecutar
                if (operacion >= 1 && operacion <= 4) {
                    //System.out.println("Se ejecuta operacion");
                    ejecutaOperacion(operacion, terminal);

                } else if (operacion == 5) { // Salir
                    System.out.println("Gracias, hasta pronto!!!");
                    break;
                } else {
                    System.out.println("Opcion no existe: " + operacion);
                }
                System.out.println();
            }
            //catch (NumberFormatException e){
            catch (Exception e){
                System.out.println();
                System.out.println("Error en ejecucion: " + e.getMessage());
                System.out.println();
            }
        }
    }

    /**
     * Funcion que uestra la opciones de menu
     */
    private static void dibujarMenu(){
        System.out.println("""
                1. Suma
                2. Resta
                3. Multiplicacion
                4. Division
                5. Salir
                """);
        System.out.print("Ingrese opcion: ");
    }

    /**
     * Funcion que ejecuta operaciones de calculadora
     * @param operacion
     * @param terminal
     */
    private static void ejecutaOperacion(int operacion, Scanner terminal){
        // Ingreso de numeros para operaciones por terminal
        System.out.print("Ingrese valor de primer numero: ");
        var numero1 = Double.parseDouble(terminal.nextLine());
        System.out.print("Ingrese valor de segundo numero: ");
        var numero2 = Double.parseDouble(terminal.nextLine());

        double resultado;

        switch (operacion) {
            case 1 -> { // Suma
                resultado = numero1 + numero2;
                System.out.println("Resultado de la suma es: " + resultado);
            }
            case 2 -> { // resta
                resultado = numero1 - numero2;
                System.out.println("Resultado de la resta es: " + resultado);
            }
            case 3 -> { // multiplicacion
                resultado = numero1 * numero2;
                System.out.println("Resultado de la multiplicacion es: " + resultado);
            }
            case 4 -> { // division
                resultado = numero1 / numero2;
                System.out.println("Resultado de la division es: " + resultado);
            }
            default -> {
                System.out.println("Opcion no existe: " + operacion);
            }
        }
    }
}
