import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class PersonasApp {
    public static void main(String[] args) {
        // Objeto para ingreso de datos por teclado
        Scanner terminal = new Scanner(System.in);

        // Definir lista
        List<Persona> personas = new ArrayList<>();

        // Menu
        var terminar = false;
        while (!terminar){
            showMenu();
            try {
                terminar = ejecutarOpciones(terminal, personas);
            }
            catch(Exception e){
                System.out.println();
                System.out.println("Error en ejecucion: " + e.getMessage());
                System.out.println();
            }

            System.out.println();
        }

    }

    /**
     * Muestra el menu principal
     */
    private static void showMenu(){
        // Opciones
        System.out.print("""
                ***** Listado de Personas 1.0 *****
                1. Agregar Persona
                2. Listar Personas
                3. Salir
                """);
        System.out.print("Ingrese opcion: ");
        System.out.println();
    }

    /**
     *  Funcion que ejecuta las opciones para el objeto persona
     * @param terminal
     * @param personas
     * @return
     */
    private static boolean ejecutarOpciones( Scanner terminal, List personas){
        var op = Integer.parseInt(terminal.nextLine());
        var salida = false;

        // verificamos la opcion ingresada
        switch (op){
            case 1 -> {
                System.out.print("Ingrese cedula: ");
                var cedula = terminal.nextLine();
                System.out.print("Ingrese nombre: ");
                var nombre = terminal.nextLine();
                System.out.print("Ingrese telefono: ");
                var telefono = terminal.nextLine();
                System.out.print("Ingrese email: ");
                var email = terminal.nextLine();

                // Crear el objeto persona
                var persona = new Persona(cedula, nombre, telefono, email);

                // Agrear el objeto al listado de personas
                personas.add(persona);
                System.out.println("El listado tienen: " + personas.size() + " personas");
            }
            case 2 -> {
                System.out.println("Listado de personas ingresadas");
                // Listar personas utilizando lambda
                //personas.forEach((persona) -> System.out.println(persona));
                personas.forEach(System.out::println);
            }
            case 3 -> {
                System.out.println("Gracias, hasta pronto!!!");
                salida = true;
            }
            default -> System.out.println("Opcion ingresada no existe");
        }

        return salida;
    }
}