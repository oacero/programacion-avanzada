import org.w3c.dom.ls.LSOutput;

public class Persona {
    // Atributos
    private int id;
    private String cedula;
    private String nombre;
    private String telefono;
    private String email;
    private static int contadorPersonas = 0;

    // Constructor por defecto

    /**
     * Constructor por defecto
     */
    public Persona(){

        this.id = ++Persona.contadorPersonas;
    }

    /**
     * Constructor con parametros
     * @param cedula
     * @param nombre
     * @param telefono
     * @param email
     */
    public Persona(String cedula, String nombre, String telefono, String email){
        this(); // llama a constructor por defecto
        this.cedula = cedula;
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
    }

    // Funciones Get y Set
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "id=" + id +
                ", cedula='" + cedula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Persona persona = new Persona("1717171717", "Maria Idrovo", "0987654321", "maria@mail.com");
        System.out.println(persona.toString());
    }


}
