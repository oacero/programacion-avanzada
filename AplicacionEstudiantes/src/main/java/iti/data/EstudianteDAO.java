package iti.data;

import iti.connect.Connect;
import iti.modelo.Estudiante;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static iti.connect.Connect.getConnect;

// Data Access Object - DAO
public class EstudianteDAO {
    public List<Estudiante> listadoEstudiantes(){
        List<Estudiante> estudiantes = new ArrayList<>();

        // Trabajo con clases de conexion a BD
        PreparedStatement ps;
        ResultSet rs;
        Connection conn = getConnect();
        String sql = "SELECT * FROM estudiante ORDER BY id_estudiante;";
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                var estudiante = new Estudiante();
                estudiante.setIdEstudiante(rs.getInt("id_estudiante"));
                estudiante.setCedula(rs.getString("cedula"));
                estudiante.setNombre(rs.getString("nombre"));
                estudiante.setApellido(rs.getString("apellido"));
                estudiante.setTelefono(rs.getString("telefono"));
                estudiante.setEmail(rs.getString("email"));
                estudiantes.add(estudiante);
            }
        }
        catch(Exception e){
            System.out.println("Ocurrio un error: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Error al cerrar conexion con BD: " + e.getMessage());
            }
        }
        return estudiantes;
    }

    public boolean findById(Estudiante estudiante){
        PreparedStatement ps;
        ResultSet rs;
        Connection conn = getConnect();
        String sql = "SELECT * FROM estudiante WHERE id_estudiante = ?;";
        try{
            ps = conn.prepareStatement(sql);
            ps.setInt(1, estudiante.getIdEstudiante());
            rs = ps.executeQuery();
            if(rs.next()){
                estudiante.setCedula(rs.getString("cedula"));
                estudiante.setNombre(rs.getString("nombre"));
                estudiante.setApellido(rs.getString("apellido"));
                estudiante.setTelefono(rs.getString("telefono"));
                estudiante.setEmail(rs.getString("email"));
                return true;
            }
        }
        catch (Exception e){
            System.out.println("Error al buscar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Error al cerrar conexion con BD: " + e.getMessage());
            }
        }
        return false;
    }

    public boolean addStudent(Estudiante estudiante){
        PreparedStatement ps;
        Connection conn = getConnect();
        String sql = "INSERT INTO estudiante (cedula, nombre, apellido, telefono, email) " +
                "VALUES(?, ?, ?, ?, ?);";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, estudiante.getCedula());
            ps.setString(2, estudiante.getNombre());
            ps.setString(3, estudiante.getApellido());
            ps.setString(4, estudiante.getTelefono());
            ps.setString(5, estudiante.getEmail());
            ps.execute();
            return true;
        }
        catch (Exception e){
            System.out.println("Error al agregar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Error al cerrar conexion con BD: " + e.getMessage());
            }
        }
        return false;
    }

    public boolean editStudent(Estudiante estudiante){
        PreparedStatement ps;
        Connection conn = getConnect();
        String sql = "UPDATE estudiante SET cedula=?, nombre=?, apellido=?, telefono=?, email=? " +
                "WHERE id_estudiante=?;";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, estudiante.getCedula());
            ps.setString(2, estudiante.getNombre());
            ps.setString(3, estudiante.getApellido());
            ps.setString(4, estudiante.getTelefono());
            ps.setString(5, estudiante.getEmail());
            ps.setInt(6, estudiante.getIdEstudiante());
            ps.execute();
            return true;
        }
        catch (Exception e){
            System.out.println("Error al actualizar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Error al cerrar conexion con BD: " + e.getMessage());
            }
        }
        return false;
    }

    public boolean deleteStudent(Estudiante estudiante) {
        PreparedStatement ps;
        Connection conn = getConnect();
        String sql = "DELETE FROM estudiante WHERE id_estudiante=?;";
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, estudiante.getIdEstudiante());
            ps.execute();
            return true;
        }
        catch (Exception e){
            System.out.println("Error al eliminar estudiante: " + e.getMessage());
        }
        finally {
            try{
                conn.close();
            }
            catch (Exception e){
                System.out.println("Error al cerrar conexion con BD: " + e.getMessage());
            }
        }
        return false;
    }

    public static void main(String[] args) {
        var estudiantedao = new EstudianteDAO();
        // Agregar estudiante
        /*var newEstudiante = new Estudiante("1717171717", "Sofia", "Espinoza", "099090909", "sofia@mail.com");
        var isAdd = estudiantedao.addStudent(newEstudiante);

        if(isAdd)
            System.out.println("Estudiante agregado exitosamente: " + newEstudiante);
        else
            System.out.println("No se pudo agregar estudiante: " + newEstudiante);*/

        // Modificar estudiante ingresado
        /*var editStudent = new Estudiante(10, "1716151413", "Sofia",
                "Espinoza", "0981234564", "sofia@mail.com");

        var isEdit = estudiantedao.editStudent(editStudent);
        if(isEdit)
            System.out.println("Estudiante actualizado exitosamente: " + editStudent);
        else
            System.out.println("No se pudo actualizado estudiante: " + editStudent);*/

        // Eliminar estudiante
        var deleteStudent = new Estudiante(6);

        var isDeleted = estudiantedao.deleteStudent(deleteStudent);
        if(isDeleted)
            System.out.println("Estudiante eliminado exitosamente: " + deleteStudent);
        else
            System.out.println("No se pudo eliminar estudiante: " + deleteStudent);

        // Listado de estudiantes
        System.out.println();
        System.out.println("Listado de Estudiantes");
        List<Estudiante> estudiantes = estudiantedao.listadoEstudiantes();
        estudiantes.forEach(System.out::println);

        // buscar por id
        /*var student = new Estudiante(6);
        //System.out.println("Antes de ejecutar busqueda por ID: " + student);
        //var existe = estudiantedao.findById(student);
        if(existe)
            System.out.println("Estudiante ubicado: " + student);
        else
            System.out.println("No se econtro estudiante con ID: " + student.getIdEstudiante());*/
    }
}
