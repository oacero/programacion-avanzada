package iti.vista;

import iti.data.EstudianteDAO;
import iti.modelo.Estudiante;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class EstudiantesApp {
    public static void main(String[] args) {
        var salir = false;
        var terminal = new Scanner(System.in);

        var estudianteDao = new EstudianteDAO();

        while(!salir){
            try {
                showMenu();
                salir = ejecutarOpciones(terminal, estudianteDao);
            }
            catch (Exception e){
                System.out.println("Error al ejecutar opciones: " + e.getMessage());
            }
            System.out.println();
        }
    }

    /**
     * Muestra el menu principal
     */
    private static void showMenu(){
        // Opciones
        System.out.print("""
                ***** Sistema de Estuadiantes 1.0 *****
                1. Listar Estudiantes
                2. Buscar Estudiante
                3. Agregar Estudiante
                4. Modificar Estuidiante
                5. Eliminar Estudiante
                6. Salir
                """);
        System.out.print("Ingrese opcion: ");
        //System.out.println();
    }

    /**
     *  Funcion que ejecuta las opciones para el objeto persona
     * @param terminal
     * @param estudianteDao
     * @return
     */
    private static boolean ejecutarOpciones( Scanner terminal, EstudianteDAO estudianteDao){
        var op = Integer.parseInt(terminal.nextLine());
        var salida = false;

        // verificamos la opcion ingresada
        switch (op) {
            case 1 -> { // Listar estudiantes
                System.out.println("Listado de Estudiantes");
                var estudiantes = estudianteDao.listadoEstudiantes();
                estudiantes.forEach(System.out::println);
            }
            case 2 -> { // Busca estudiantge por ID
                System.out.print("Ingrese ID de estudiante a buscar: ");
                var idEstudiante = Integer.parseInt(terminal.nextLine());
                var estudiante = new Estudiante(idEstudiante);
                var existeEstudiante = estudianteDao.findById(estudiante);
                if(existeEstudiante)
                    System.out.println("Estudiante encontrado: " + estudiante);
                else
                    System.out.println("No se encontro estudiante con ID: " + estudiante.getIdEstudiante());
            }
            case 3 -> { // Agregar estudiante
                System.out.println("Agregar Estudiante");
                System.out.print("Cedula: ");
                var cedula =  terminal.nextLine();
                System.out.print("Nombre: ");
                var nombre =  terminal.nextLine();
                System.out.print("Apellido: ");
                var apellido =  terminal.nextLine();
                System.out.print("Telefono: ");
                var telefono =  terminal.nextLine();
                System.out.print("Email: ");
                var email =  terminal.nextLine();

                var newEstudiante = new Estudiante(cedula, nombre, apellido, telefono, email);
                var isAdd = estudianteDao.addStudent(newEstudiante);
                if(isAdd)
                    System.out.println("Estudiante agregado exitosamente: " + newEstudiante);
                else
                    System.out.println("No se pudo agregar estudiante: " + newEstudiante);
            }
            case 4 -> {// Modificar estudiante
                System.out.println("Modificar Estudiante");
                System.out.print("Ingrese ID de estudiante a modificar: ");
                var idEstudiante = Integer.parseInt(terminal.nextLine());
                System.out.print("Cedula: ");
                var cedula =  terminal.nextLine();
                System.out.print("Nombre: ");
                var nombre =  terminal.nextLine();
                System.out.print("Apellido: ");
                var apellido =  terminal.nextLine();
                System.out.print("Telefono: ");
                var telefono =  terminal.nextLine();
                System.out.print("Email: ");
                var email =  terminal.nextLine();

                var editStudent = new Estudiante(idEstudiante, cedula, nombre, apellido, telefono, email);
                var isEdit = estudianteDao.editStudent(editStudent);
                if(isEdit)
                    System.out.println("Estudiante actualizado exitosamente: " + editStudent);
                else
                    System.out.println("No se pudo actualizado estudiante: " + editStudent);
            }
            case 5 -> {// Eliminar estudiante
                System.out.println("Eliminar Estudiante");
                System.out.print("Ingrese ID de estudiante a eliminar: ");
                var idEstudiante = Integer.parseInt(terminal.nextLine());
                var deleteStudent = new Estudiante(idEstudiante);

                var isDeleted = estudianteDao.deleteStudent(deleteStudent);
                if(isDeleted)
                    System.out.println("Estudiante eliminado exitosamente: " + deleteStudent);
                else
                    System.out.println("No se pudo eliminar estudiante: " + deleteStudent);
            }
            case 6 -> {
                System.out.println("Gracias, hasta pronto!!!");
                salida = true;
            }
            default -> System.out.println("Opcion ingresada no existe");

        }
        return salida;
    }


}