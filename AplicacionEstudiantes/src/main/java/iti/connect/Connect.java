package iti.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {

    /**
     * Clase de conexion a la base de datos
     * @return
     */
    public static Connection getConnect(){
        Connection connect = null;

        var database = "base_estudiantes";
        var url = "jdbc:mysql://localhost:3306/" + database;
        var username = "root";
        var password = "";

        // Llamada a driver de conexion
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connect = DriverManager.getConnection(url, username, password);
        }
        //catch(Exception e){
        catch(ClassNotFoundException | SQLException e){
            System.out.println("Error en conexion a base de datos: " + e.getMessage());
        }

        return connect;
    }

    public static void main(String[] args) {
        var conexion = Connect.getConnect();
        if(conexion != null)
            System.out.println("Conexion a BD satisfactoria!!! " + conexion);
        else
            System.out.println("Existio un error al conectarse");
    }
}
