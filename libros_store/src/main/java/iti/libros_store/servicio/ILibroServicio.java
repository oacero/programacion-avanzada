package iti.libros_store.servicio;

import iti.libros_store.modelo.Libro;
import java.util.List;

public interface ILibroServicio {
    public List<Libro> listaLibros();
    public Libro BucarLibroById(Integer idLibro);
    public void guardaLibro(Libro libro);
    public void eliminaLibro(Libro libro);

}
