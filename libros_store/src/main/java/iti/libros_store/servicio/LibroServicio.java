package iti.libros_store.servicio;

import iti.libros_store.modelo.Libro;
import iti.libros_store.repositorio.LibroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroServicio implements ILibroServicio{
    @Autowired
    private LibroRepositorio libroRepositorio;
    @Override
    public List<Libro> listaLibros() {
        return libroRepositorio.findAll();
    }

    @Override
    public Libro BucarLibroById(Integer idLibro) {
         Libro libro = libroRepositorio.findById(idLibro).orElse(null);
        return libro;
    }

    @Override
    public void guardaLibro(Libro libro) {
        libroRepositorio.save(libro);
    }

    @Override
    public void eliminaLibro(Libro libro) {
        libroRepositorio.delete(libro);
    }
}
