package iti.libros_store;

import iti.libros_store.vista.LibroForm;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.awt.*;

@SpringBootApplication
public class LibrosStoreApplication {

	public static void main(String[] args) {

		//SpringApplication.run(LibrosStoreApplication.class, args);
		ConfigurableApplicationContext contextoSpring =
				new SpringApplicationBuilder(LibrosStoreApplication.class)
						.headless(false)
						.web(WebApplicationType.NONE)
						.run(args);

		// Ejecucion de formulario
		EventQueue.invokeLater(()->{
			// Obtener objeto formulario por medio de Spring
			LibroForm libroForm = contextoSpring.getBean(LibroForm.class);
			libroForm.setVisible(true);
		});
	}

}
