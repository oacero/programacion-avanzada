package iti.libros_store.vista;

import iti.libros_store.modelo.Libro;
import iti.libros_store.servicio.LibroServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@Component
public class LibroForm extends JFrame {
    LibroServicio libroServicio;
    private JPanel panel;
    private JTable tablaLibros;
    private JTextField idTexto;
    private JTextField libroTexto;
    private JTextField autorTexto;
    private JTextField precioTexto;
    private JTextField textoExistencia;
    private JButton agregarButton;
    private JButton modificarButton;
    private JButton eliminarButton;
    private DefaultTableModel tableModelLibros;

    @Autowired
    public LibroForm(LibroServicio libroServicio){
        this.libroServicio = libroServicio;
        initForm();
        agregarButton.addActionListener(e -> agregarLibro());
        tablaLibros.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                loadLibroSelected();
            }
        });
        modificarButton.addActionListener(e -> modificaLibro());
        eliminarButton.addActionListener(e -> eliminarLibro());
    }

    private void initForm(){
        setContentPane(panel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(800, 600);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension sizeScreen = toolkit.getScreenSize();
        int x = (sizeScreen.width - getWidth()/2);
        int y = (sizeScreen.height - getHeight()/2);
        setLocation(x, y);
    }

    private void agregarLibro(){
        // Obtener valores del formulario
        if(libroTexto.getText().equals("")){
            showMessage("Ingrese el nombre del libro");
            libroTexto.requestFocusInWindow();
            return;
        }
        var nombreLibro = libroTexto.getText();
        var autor = autorTexto.getText();
        var precio = Double.parseDouble(precioTexto.getText());
        var existencia = Integer.parseInt(textoExistencia.getText());

        // Crear objeto tipo libro
        var libro = new Libro(null, nombreLibro, autor, precio, existencia);
        /*libro.setNombreLibro(nombreLibro);
        libro.setAutor(autor);
        libro.setPrecio(precio);
        libro.setExistencia(existencia);*/
        this.libroServicio.guardaLibro(libro);
        showMessage("Libro agregado exitosamente!!!");
        cleanForm();
        listaLibros();
    }

    private void showMessage(String msg){
        JOptionPane.showMessageDialog(this, msg);
    }

    private void cleanForm(){
        libroTexto.setText("");
        autorTexto.setText("");
        precioTexto.setText("");
        textoExistencia.setText("");
    }

    private void loadLibroSelected(){
        // indices de filas empiezan en 0
        var fila = tablaLibros.getSelectedRow();
        if(fila != -1){ // retorna -1 si no selecciono registro
            String idLibro = tablaLibros.getModel().getValueAt(fila, 0).toString();
            idTexto.setText(idLibro);
            String nombreLibro = tablaLibros.getModel().getValueAt(fila, 1).toString();
            libroTexto.setText(nombreLibro);
            String autor = tablaLibros.getModel().getValueAt(fila, 2).toString();
            autorTexto.setText(autor);
            String precio = tablaLibros.getModel().getValueAt(fila, 3).toString();
            precioTexto.setText(precio);
            String existencia = tablaLibros.getModel().getValueAt(fila, 4).toString();
            textoExistencia.setText(existencia);
        }
    }

    private void modificaLibro(){
        if(idTexto.getText().equals("")){
            showMessage("Por favor, seleccione libro a modificar!!!");
        }
        else{
            // Verificar nombre libro
            if(libroTexto.getText().equals("")){
                showMessage("Ingrese el nombre del libro");
                libroTexto.requestFocusInWindow();
                return;
            }
            // seteamos los datos del libro a actualizar
            int idLibro = Integer.parseInt(idTexto.getText());
            var nombreLibro = libroTexto.getText();
            var autor = autorTexto.getText();
            var precio = Double.parseDouble(precioTexto.getText());
            var existencia = Integer.parseInt(textoExistencia.getText());

            var libro = new Libro(idLibro, nombreLibro, autor, precio, existencia);
            this.libroServicio.guardaLibro(libro);
            showMessage("Libro modificado exitosamente!!!");
            cleanForm();
            listaLibros();
        }
    }

    private void eliminarLibro(){
        var fila = tablaLibros.getSelectedRow();
        if(fila != -1) { // retorna -1 si no selecciono registro
            String idLibro = tablaLibros.getModel().getValueAt(fila, 0).toString();
            var libro = new Libro();
            libro.setIdLibro(Integer.parseInt(idLibro));
            this.libroServicio.eliminaLibro(libro);
            showMessage("Libro " + idLibro + " eliminado exitosamente!!!");
            cleanForm();
            listaLibros();
        }
        else {
            showMessage("Por favor, seleccione libro a eliminar...");
        }
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        // Crear elemento de idTexto oculto
        idTexto = new JTextField("");
        idTexto.setVisible(false);

        this.tableModelLibros = new DefaultTableModel(0, 5){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };

        String[] header = {"Id", "Libro", "Autor", "Precio", "Existencia"};
        this.tableModelLibros.setColumnIdentifiers(header);
        // Intanciamos objeto JTable
        this.tablaLibros = new JTable(tableModelLibros);
        // Desactivar seleccion multiple de registros
        tablaLibros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listaLibros();
    }

    private void listaLibros(){
        // Limpiar tabla
        tableModelLibros.setRowCount(0);

        // Get libros
        var libros = libroServicio.listaLibros();
        libros.forEach((libro)->{
            Object[] filaLibro = {
              libro.getIdLibro(),
              libro.getNombreLibro(),
              libro.getAutor(),
              libro.getPrecio(),
              libro.getExistencia()
            };
            this.tableModelLibros.addRow(filaLibro);
        });

    }

}
